<?php

namespace shuvaevsb\slides;

use shuvaevsb\slides\assets\ResponsiveSlidesAsset;
use yii\base\Widget;
use yii\helpers\Json;

class ResponsiveSlides extends Widget
{
    // array options to populate Slick jQuery object
    public $clientOptions = [];

    // array elements for the carousel
    public $images = [];
    
    // array elements for the pager
    public $thumbnails = [];

    public $template;
    
    public $container = 'rslides';
    
    public $etap=1;

    public function init()
    {

    }

    /**
     * Register required scripts for the Slick plugin
     */
    protected function registerClientScript()
    {
        $view = $this->getView();

        ResponsiveSlidesAsset::register($view);

        $options = Json::encode($this->clientOptions);

        $js[] = ";";

        $js[] = "jQuery('.$this->container').responsiveSlides({$options});";

        $view->registerJs(implode(PHP_EOL, $js));
    }

    public function run()
    {
        echo $this->renderFile('@vendor/shuvaevsb/yii2-responsiveslides/views/slider_'.$this->template.'.php');

        $this->registerClientScript();

    }
}