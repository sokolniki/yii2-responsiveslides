<?php

use yii\helpers\Html;

echo Html::beginTag('ul', ['class' => 'rslides', 'id' => 'slider2']);

foreach ($this->context->images as $image)
{
    echo Html::beginTag('li');

    echo Html::tag('a', $image);

    echo Html::endTag('li');
}
echo Html::endTag('ul');

/*
 * 'auto' => false,
 * 'pager' => true,
 * 'speed' => 300,
 * 'maxwidth' => 540
 * */
